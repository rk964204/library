### Library frontend

Steps to run frontend (run all the commands in current directory)

1. Install required packages by running `npm install`
2. Run server by running command `npm run start`

Go to url `localhost:3000`
