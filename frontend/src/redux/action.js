import {
    ADD_NEW_BOOK_DETAILS,
    SET_BOOKS_DETAILS, SET_IS_ADDING_BOOKS,
    SET_IS_FETCHING_BOOKS, SET_IS_UPDATING_BOOKS, SET_TOAST_MESSAGE, UPDATE_BOOK_DETAILS
} from "./actionTypes";
import {addNewBookRpc, fetchBooksDetailsRpc, updateBookRpc} from "../rpc";

const setIsFetchingBooks = isFetching => ({
    type: SET_IS_FETCHING_BOOKS,
    payload: {
        isFetching
    }
});

const setIsUpdatingBook = isUpdating => ({
    type: SET_IS_UPDATING_BOOKS,
    payload: {
        isUpdating
    }
});

const setIsAddingBook = isAdding => ({
    type: SET_IS_ADDING_BOOKS,
    payload: {
        isAdding
    }
});

const setBooksDetails = (details, error) => ({
    type: SET_BOOKS_DETAILS,
    payload: {
        details,
        error,
    }
});

const addNewBookDetails = (details, error) => ({
    type: ADD_NEW_BOOK_DETAILS,
    payload: {
        details,
        error,
    }
});

const updateBookDetails = (details, error) => ({
    type: UPDATE_BOOK_DETAILS,
    payload: {
        details,
        error,
    }
});

export const setToastMessage = (message, type = null) => ({
    type: SET_TOAST_MESSAGE,
    payload: {
        message,
        type,
    }
})

export const fetchBooksDetails = (query, id = null) => {
    return async (dispatch, getState) => {
        dispatch(setIsFetchingBooks(true));
        const {details, error} = await fetchBooksDetailsRpc(query, id);
        if (error) {
            dispatch(setToastMessage('Failed to fetch book details!', 'ERROR'));
        }
        dispatch(setIsFetchingBooks(false));
        dispatch(setBooksDetails(details, error));
    };
}

export const addNewBook = data => {
    return async (dispatch, getState) => {
        dispatch(setIsAddingBook(true));
        const {details, error} = await addNewBookRpc(data);
        if (error) {
            dispatch(setToastMessage('Failed to add book details!', 'ERROR'));
        } else {
            dispatch(setToastMessage('Book details added successfully!', 'SUCCESS'));
        }
        dispatch(setIsAddingBook(false));
        dispatch(addNewBookDetails(details, error));
    };
}

export const updateBook = data => {
    return async (dispatch, getState) => {
        dispatch(setIsUpdatingBook(true));
        const {details, error} = await updateBookRpc(data);
        if (error) {
            dispatch(setToastMessage('Failed to update book details!', 'ERROR'));
        } else {
            dispatch(setToastMessage('Book details updated successfully!', 'SUCCESS'));
        }
        dispatch(setIsUpdatingBook(false));
        dispatch(updateBookDetails(details, error));
    };
}

