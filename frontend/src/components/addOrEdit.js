import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {addNewBook, fetchBooksDetails, updateBook} from "../redux/action";
import Loader from "react-loader";

const emptyDetails = {
    id: null,
    name: '',
    description: '',
    author: '',
    count: '',
};

function AddOrEdit(props) {
    const [details, setDetails] = useState(emptyDetails);

    useEffect(() => {
        if (props.editBookId && !props.defaultDetails) {
            props.fetchBooksDetails(null, props.editBookId);
        }
    }, [])

    useEffect(() => {
        if (props.defaultDetails)
            setDetails(props.defaultDetails);
    }, [props.defaultDetails])

    const addOrUpdateDetails = () => {
        const data = {
            ...details,
            count: parseInt(details.count),
        }
        if (details?.id) {
            props.updateBook(data);
        } else {
            props.addNewBook(data);
            setDetails(emptyDetails);
        }
    }

    const onClickShowAllBooks = () => {
        props.history.push('/books/details');
    }

    const isValidDetails = () => {
        if (!details?.name?.trim()?.length)
            return false;
        if (!details?.description?.trim()?.length)
            return false;
        if (!details?.author?.trim()?.length)
            return false;
        const count = parseInt(details?.count);
        if (!(Number.isInteger(count) && count >= 0))
            return false;
        return true;
    }

    const renderForm = () => {
        return (
            <>
                <div style={styles.formContainer}>
                    <div style={styles.labelContainer}>
                        <label>{'Name'}</label>
                    </div>
                    <div style={styles.textContainer}>
                        <input
                            style={styles.inputTextField}
                            value={details?.name}
                            onChange={(event) => setDetails(
                                {...details, name: event.target.value}
                            )}
                        />
                    </div>
                </div>
                <div style={styles.formContainer}>
                    <div style={styles.labelContainer}>
                        <label>{'Description'}</label>
                    </div>
                    <div style={styles.textContainer}>
                        <textarea
                            style={{...styles.inputTextField, height: 100}}
                            value={details?.description}
                            onChange={(event) => setDetails(
                                {...details, description: event.target.value}
                            )}
                        />
                    </div>
                </div>
                <div style={styles.formContainer}>
                    <div style={styles.labelContainer}>
                        <label>{'Author'}</label>
                    </div>
                    <div style={styles.textContainer}>
                        <input
                            style={styles.inputTextField}
                            value={details?.author}
                            onChange={(event) => setDetails(
                                {...details, author: event.target.value}
                            )}
                        />
                    </div>
                </div>
                <div style={styles.formContainer}>
                    <div style={styles.labelContainer}>
                        <label>{'Count'}</label>
                    </div>
                    <div style={styles.textContainer}>
                        <input
                            style={styles.inputTextField}
                            value={details?.count}
                            onChange={(event) => setDetails(
                                {...details, count: event.target.value}
                            )}
                            type={'number'}
                            min={0}
                        />
                    </div>
                </div>
                <div style={styles.formContainer}>
                    <div style={styles.textContainer}>
                        <button
                            onClick={addOrUpdateDetails}
                            disabled={!isValidDetails()}
                            style={
                                !isValidDetails() ?
                                    {...styles.button, ...styles.addUpdateButton, ...styles.buttonDisabled} :
                                    {...styles.button, ...styles.addUpdateButton}
                            }
                        >
                            {props.editBookId ? 'Update': 'Add'}
                        </button>
                    </div>
                </div>
                <div style={styles.formContainer}>
                    <div style={styles.textContainer}>
                        <button
                            onClick={onClickShowAllBooks}
                            style={{...styles.button, ...styles.detailsButton}}
                        >
                            {'Show all books'}
                        </button>
                    </div>
                </div>
            </>
        );
    }

    return (
        <div style={{flex: 1, width: '100%'}}>
            {props.isFetching && <Loader />}
            {!props.isFetching && !props.error && renderForm()}
        </div>
    );
}

const styles = {
    formContainer: {
        flex: 1,
        display: 'flex'
    },
    labelContainer: {
        width: '80px',
        padding: '28px 5px',
        textAlign: 'right',
        height: 30,
    },
    textContainer: {
        flex: 1,
        padding: 10,
    },
    inputTextField: {
        height: 30,
        width: 200,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 10,
        marginRight: 10,
        padding: 5,
    },
    button: {
        border: 'none',
        borderRadius: 5,
        padding: '10px 0px',
        textAlign: 'center',
        display: 'inline-block',
        fontSize: 16,
        marginTop: 10,
        marginRight: 10,
        cursor: 'pointer',
        width: 150
    },
    buttonDisabled: {
        cursor: 'not-allowed',
        opacity: 0.6,
    },
    addUpdateButton: {
        backgroundColor: '#4CAF50',
        color: '#FFFFFF',
        marginLeft: 90,
    },
    detailsButton: {
        backgroundColor: '#DEDEDE',
        color: '#000000',
        marginLeft: 90,
    }
}

function mapStateToProps(state, props) {
    let editBookId = null;
    if (props?.match?.params?.bookId) {
        editBookId = parseInt(props?.match?.params?.bookId);
    }
    const defaultDetails = (state.books?.data || []).find(item => item.id === editBookId);

    return {
        editBookId,
        defaultDetails,
        isFetching: state.books?.isFetching,
        error: state.books?.error,
        isAddingOrUpdating: state.books?.isAddingOrUpdating,
        errorAddingOrUpdating: state.books?.errorAddingOrUpdating,
    }
}

export default connect(
    mapStateToProps,
    {
        fetchBooksDetails,
        addNewBook,
        updateBook
    }
)(AddOrEdit);

