import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    Link,
} from "react-router-dom";
import ShowBookDetails from "./showBookDetails";
import AddOrEdit from "./addOrEdit";
import Toast from "./toast";

export default function App(props) {
    return (
        <>
            <Router>
                <Switch>
                    <Route exact={true} path="/books/details" component={ShowBookDetails} />
                    <Route exact={true}  path="/books/add" component={AddOrEdit} />
                    <Route exact={true}  path="/books/edit/:bookId" component={AddOrEdit} />
                    <Redirect exact={true} from="/" to="/books/details" />
                    <Route component={NotFound} />
                </Switch>
            </Router>
            <Toast />
        </>
    );
}

const NotFound = () => (
    <div>
        <h1>404 - Not Found!</h1>
        <Link to="/">
            Go Home
        </Link>
    </div>
);
