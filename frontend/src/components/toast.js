import React, {useEffect} from "react";
import {connect} from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {setToastMessage} from "../redux/action";

function Toast(props) {
    useEffect(() => {
        if (props.message) {
            if (props.type === 'SUCCESS')
                toast.success(props.message);
            else if (props.type === 'ERROR')
                toast.error(props.message);
            else
                toast.info(props.message);
            props.setToastMessage(null);
        }
    }, [props.message])

    return (
        <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
        />
    )
}

function mapStateToProps(state, props) {
    return {
        message: state.ui?.toastMsg,
        type: state.ui?.toastType,
    }
}

export default connect(
    mapStateToProps,
    {
        setToastMessage,
    }
)(Toast);
