import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import Loader from 'react-loader';
import {fetchBooksDetails} from "../redux/action";

function ShowBookDetails(props) {
    const [searchText, setSearchText] = useState(null);
    const [selectedRow, setSelectedRow] = useState(null);

    useEffect(() => {
        props.fetchBooksDetails(null)
    }, []);

    const onClickSearch = () => {
        props.fetchBooksDetails(searchText);
    }

    const onClickClear = () => {
        setSearchText("");
        props.fetchBooksDetails(null);
    }

    const onClickAddButton = () => {
        props.history.push('/books/add');
    }

    const onClickEditButton = () => {
        const books = props.books || [];
        console.log(books);
        console.log(selectedRow);
        if (selectedRow !== null && selectedRow < books.length) {
            props.history.push(`/books/edit/${books[selectedRow].id}`);
        }
    }

    const displayBookDetails = () => {
        if (props.isFetching) {
            return (<Loader />);
        }
        return (
            <table style={{flex: 1, width: '100%', marginTop: 20}}>
                <tr style={{flex: 1, width: '100%'}}>
                    <th style={{...styles.tableCell, ...styles.tableHead, width: '10%'}}>S.No.</th>
                    <th style={{...styles.tableCell, ...styles.tableHead}}>Name</th>
                    <th style={{...styles.tableCell, ...styles.tableHead, width: '40%'}}>Description</th>
                    <th style={{...styles.tableCell, ...styles.tableHead}}>Author</th>
                    <th style={{...styles.tableCell, ...styles.tableHead, width: '10%'}}>Count</th>
                </tr>
                {
                    (props.books || []).map((item, index) => {
                        let cellBackgroundColor = '#FFFFFF';
                        if (index % 2) {
                            cellBackgroundColor = '#EEEEEE';
                        }
                        if (index === selectedRow) {
                            cellBackgroundColor = '#AAAAAA';
                        }
                        return (
                            <tr onClick={() => setSelectedRow(index)} style={{cursor: 'pointer'}}>
                                <td style={{...styles.tableCell, backgroundColor: cellBackgroundColor, width: '10%'}}>{index+1}</td>
                                <td style={{...styles.tableCell, backgroundColor: cellBackgroundColor}}>{item?.name}</td>
                                <td style={{...styles.tableCell, backgroundColor: cellBackgroundColor, width: '40%'}}>{item?.description}</td>
                                <td style={{...styles.tableCell, backgroundColor: cellBackgroundColor}}>{item?.author}</td>
                                <td style={{...styles.tableCell, backgroundColor: cellBackgroundColor, width: '10%'}}>{item?.count}</td>
                            </tr>
                        )
                    })
                }
            </table>
        )
    }

    const renderSearchBar = () => {
        const isButtonDisabled = (
            props.isFetching || searchText === null || searchText.length === 0
        );
        return (
            <div style={{flex: 1, padding: 5}}>
                <input
                    style={styles.searchBox}
                    disabled={props.isFetching}
                    placeholder={"Enter keywords"}
                    type="text"
                    value={searchText}
                    onChange={(event) => setSearchText(event.target.value)}
                />
                <button
                    style={ isButtonDisabled ?
                        {...styles.button, ...styles.searchButton, ...styles.buttonDisabled} :
                        {...styles.button, ...styles.searchButton}
                    }
                    onClick={onClickSearch}
                    disabled={isButtonDisabled}
                >
                    {"Search"}
                </button>
                <button
                    style={{...styles.button, ...styles.clearButton}}
                    onClick={onClickClear}
                >
                    {"Clear"}
                </button>
                <button
                    style={{...styles.button, ...styles.clearButton}}
                    onClick={onClickAddButton}
                >
                    {"Add"}
                </button>
                <button
                    style={
                        selectedRow !== null ?
                        {...styles.button, ...styles.clearButton} :
                        {...styles.button, ...styles.buttonDisabled, ...styles.clearButton}
                    }
                    onClick={onClickEditButton}
                    disabled={selectedRow === null}
                >
                    {"Edit"}
                </button>
            </div>
        );
    }

    return (
        <div style={{flex: 1, width: '100%'}}>
            {renderSearchBar()}
            {displayBookDetails()}
        </div>
    );
}

function mapStateToProps(state) {
    return {
        isFetching: state.books?.isFetching,
        books: state.books?.data,
        error: state.books?.error,
    }
}

export default connect(
    mapStateToProps,
    {
        fetchBooksDetails
    }
)(ShowBookDetails);

const styles = {
    tableHead: {
        backgroundColor: '#4CAF50',
        color: '#FFFFFF',
    },
    tableCell: {
        width: '20%',
        padding: 10,
        borderWidth: 1,
        textAlign: 'center',
        minHeight: 30,
    },
    searchBox: {
        height: 32,
        width: 215,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 10,
        marginRight: 10,
        paddingLeft: 5,
        paddingRight: 5,
    },
    button: {
        border: 'none',
        borderRadius: 5,
        padding: '10px 0px',
        textAlign: 'center',
        display: 'inline-block',
        fontSize: 16,
        marginTop: 10,
        marginRight: 10,
        cursor: 'pointer',
        width: 108
    },
    buttonDisabled: {
        cursor: 'not-allowed',
        opacity: 0.6,
    },
    searchButton: {
        backgroundColor: '#4CAF50',
        color: '#FFFFFF',
    },
    clearButton: {
        backgroundColor: '#DEDEDE',
        color: '#000000',
    }
}
