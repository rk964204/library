import {SET_TOAST_MESSAGE} from "../redux/actionTypes";

const initialState = {
    toastMsg: null,
    toastType: null
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_TOAST_MESSAGE: {
            const {message, type} = action.payload;
            return {
                ...state,
                toastMsg: message,
                toastType: type,
            };
        }
        default:
            return state;
    }
}
