import { combineReducers } from 'redux';
import booksReducer from './books';
import uiReducer from './ui';

export const rootReducer = combineReducers({
    books: booksReducer,
    ui: uiReducer,
});
