import {SET_BOOKS_DETAILS, SET_IS_FETCHING_BOOKS} from "../redux/actionTypes";

const initialState = {
    isFetching: false,
    error: null,
    data: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_IS_FETCHING_BOOKS: {
            const {isFetching} = action.payload;
            return {
                ...state,
                isFetching: isFetching
            };
        }
        case SET_BOOKS_DETAILS: {
            const {details, error} = action.payload;
            return {
                ...state,
                data: details,
                error: error,
                isFetching: false
            };
        }
        default:
            return state;
    }
}
