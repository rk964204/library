const HOST = 'http://localhost:8081'

export const fetchBooksDetailsRpc = async (query, id) => {
    let endpoint = `${HOST}/api/books/details`;
    if (id) {
        endpoint = `${endpoint}?id=${id}`;
    }
    else if (query) {
        endpoint = `${endpoint}?q=${query}`;
    }
    try {
        const response = await fetch(endpoint);
        if (response.status !== 200) {
            return {
                details: [],
                error: response
            }
        }
        const data = await response.json();
        return {details: data, error: null};
    } catch (e) {
        return {details: null, error: e};
    }
};

export const addNewBookRpc = async (data) => {
    const endpoint = `${HOST}/api/books/add`;
    try {
        const response = await fetch(endpoint, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        if (response.status !== 200) {
            return {
                details: [],
                error: response
            }
        }
        const details = await response.json();
        return {details: details, error: null};
    } catch (e) {
        return {details: null, error: e};
    }
};

export const updateBookRpc = async (data) => {
    const endpoint = `${HOST}/api/books/edit`;
    try {
        const response = await fetch(endpoint, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        if (response.status !== 200) {
            return {
                details: [],
                error: response
            }
        }
        const details = await response.json();
        return {details: details, error: null};
    } catch (e) {
        return {details: null, error: e};
    }
};
