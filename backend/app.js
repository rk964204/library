const express = require('express');
const cors = require('cors');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(cors({
    origin: 'http://localhost:3000'
}));



app.post('/api/books/add', async function(req, res) {
    const dataStr = await fs.readFileSync( __dirname + "/" + "book.json", 'utf8');
    const data = JSON.parse(dataStr);

    const details = {
        id: data.length + 1,
        name: req.body.name,
        description: req.body.description,
        count: req.body.count,
        author: req.body.author,
    }
    data.push(details);

    fs.writeFileSync( __dirname + "/" + "book.json", JSON.stringify(data));
    res.end(JSON.stringify(details));
})

app.post('/api/books/edit', async function(req, res) {
    const dataStr = await fs.readFileSync( __dirname + "/" + "book.json", 'utf8');
    const data = JSON.parse(dataStr);

    const details = {
        id: req.body.id,
        name: req.body.name,
        description: req.body.description,
        count: req.body.count,
        author: req.body.author,
    };
    const index = data.findIndex(item => item.id === details.id);
    if (index === -1) {
        res.status(400).send();
    }
    data[index] = details;
    fs.writeFileSync( __dirname + "/" + "book.json", JSON.stringify(data));
    res.end(JSON.stringify(details));
})

app.get('/api/books/details', async function(req, res) {
    const dataStr = await fs.readFileSync( __dirname + "/" + "book.json", 'utf8');
    const data = JSON.parse(dataStr);
    if (req.query.id) {
        const index = data.findIndex(item => item.id.toString() === req.query.id);
        if (index === -1) {
            res.status(400).end();
        } else {
            res.end(JSON.stringify([data[index]]));
        }
    } else if (req.query.q) {
        let result = []
        const q = req.query.q.toLowerCase();
        for (let index in data) {
            const book = data[index];
            if (book.name.toLowerCase().includes(q)) {
                result.push(book);
            } else if (book.author.toLowerCase().includes(q)) {
                result.push(book);
            } else if (book.description.toLowerCase().includes(q)) {
                result.push(book);
            }
        }
        res.end(JSON.stringify(result));
    } else {
        res.end(JSON.stringify(data));
    }
})

const server = app.listen(8081, function () {
    const host = server.address().address
    const port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
})
