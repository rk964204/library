### Library Backend

Steps to run backend server (run all the commands in current directory)

1. Install required packages by running `npm install`
2. Run server by running command `node app.js`

There are three api in app.js file:

1.. `/api/books/add` - `POST` method

Request: 

curl -i -X POST \
   -H "Content-Type:application/json" \
   -d \
'{
"name":"Programming in ANSI C",
"description":"This seventh edition is thoroughly updated with outcome based learning approach as per standard Bloom’s Taxonomy. The new additions are important contents like “Graphic programming using C. Self-explanatory interactive simulation videos and case studies are integrated throughout the book using QR codes. Additional write-ups and projects are also available for reference of the user.",
  "count":20,
  "author":" Balagurusamy"
}' \
 'http://127.0.0.1:8081/api/books/add'

Response:

{
"name":"Programming in ANSI C",
"description":"This seventh edition is thoroughly updated with outcome based learning approach as per standard Bloom’s Taxonomy. The new additions are important contents like “Graphic programming using C. Self-explanatory interactive simulation videos and case studies are integrated throughout the book using QR codes. Additional write-ups and projects are also available for reference of the user.",
  "count":20,
  "author":" Balagurusamy"
}


2.. `/api/books/edit` - `POST` method

Request: 

curl -i -X POST \
   -H "Content-Type:application/json" \
   -d \
'{
"name":"Rich Dad Poor",
"description":"It\''s been nearly 25 years since Robert Kiyosaki’s Rich Dad Poor Dad first made waves in the Personal Finance arena. It has since become the #1 Personal Finance book of all time... translated into dozens of languages and sold around the world.",
  "count":50,
  "author":"Robert T. Kiyosaki",
  "id":2
}' \
 'http://127.0.0.1:8081/api/books/edit'

Response:

{"id":2,"name":"Rich Dad Poor","description":"It's been nearly 25 years since Robert Kiyosaki’s Rich Dad Poor Dad first made waves in the Personal Finance arena. It has since become the #1 Personal Finance book of all time... translated into dozens of languages and sold around the world.","count":50,"author":"Robert T. Kiyosaki"}

3.. `/api/books/details` - `GET` method

Request

curl -i -X GET \
 'http://127.0.0.1:8081/api/books/details?q=Hogwarts'

Response

[{"id":1,"name":"Harry Potter and the Philosopher's Stone","description":"Escape to Hogwarts with the unmissable series that has sparked a lifelong reading journey for children and families all over the world. The magic starts here.\nHarry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry's eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. The magic starts here!\nThese editions of the classic and internationally bestselling Harry Potter series feature thrilling jacket artwork by award-winning illustrator Jonny Duddle. They are the perfect starting point for anyone who's ready to lose themselves in the greatest children's story of all time.","count":15,"author":"J.K. Rowling!!!!"}]

Every data related to book has stored in book.json file which you can find in the directory.

Requirements:

`Node version - 14.5.0`
